# IFMS: Integrated Framework for Mathematical Simulation

A complex simulation framework that takes into account:

* of generating simulation layout(s)
* environment description file: Location of SUMO binaries
* SUMO Node Configuration File: The coordinates of objects in a layout
* Simulate and visualize the model