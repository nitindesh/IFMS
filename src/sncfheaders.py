#!C:\Python27
#!/usr/bin/python
# Name:sncfheaders.py
# SUMO:(S)imulation of (U)rban (MO)bility (SUMO)
# Author:Nitin Deshpande
# In compliance with Version 0.1.4 rev 1 of the SNCF Format.
# What does this program do?
# The .sncf (SUMO Node Configuration File) has fields (meta data). The meta-data contains:
# type,node identifiers,author,created date and comments/documentation. This program helps in specifying
# meta-data.
# Added feature of statistics. 

# Reason List for changes and version.
# [Reason 1] In compliance with Ver 0.1.4 rev 1 of the SNCF Format.
# [Reason 2] In compliance with Ver 0.1.4 of the SNCF Format and to keep in terms with efficient coding and file format.
# [Reason 3] In compliance with Ver 0.1.4 rev 2 of the SNCF Format.
# [Reason 4] In compliance with Ver 0.1.4 rev 3 of the SNCF Format.
import os
import time
import datetime

# SNCF Header functions

def readIgnore ():
    '''
Read data and do nothing.
    '''
    pass

def printNew ():
    ''' 
A simple function that returns a new line character.
    '''
    return "\n"

def waitForSync (numOfSec):
    '''
Waits for numOfSec seconds (Integral value). Used for synchronisation purposes.
    '''
    for ticks in range (1,numOfSec):
        time.sleep(1)
    print '[waitForSync says]: Done'
    return None

#SNCF FUNCTIONS

def makeSNCF (fileString):
    '''
Create a normal SNCF File (Sumo Node Configuration File) with all headers intact,
but without metadata.
    '''
    # File constants
    fileExt     = '.sncf'
    slashChar   = '\\'
    fileType    = "@type:ascii"
    fileNSID    = "@nsi:"
    fileAuthor  = "@author:"
    fileCreated = "@created:"

    # fileDocument= "\'\'\' TODO: <Documentation/Comment here> \'\'\'"
    beginCoord   = "@start_coord_node_id_(x,y)"
    # addCoord = "#<nsid>,<add co-ordinates here>" [Reason 2]
    endCoord = "@end_coord"
    # [Reason 4] In compliance with Ver 0.1.4 rev 3 of the SNCF Format.
    # The reasons for the following from <@start_elist> until <@end_vehID> addendum is [Reason 1]. Look above for explanations of reasons.
    beginElist = "@start_elist_(edgName,frmId,toId)" # [Reason 1]
    endElist = "@end_elist"
	# [Reason 4] Look at the "Reason List" on top for explanations.
    beginRouteEntry="@start_routeentry_({routeID,[R G B],[edgName_a edgName_b edgName_c  edgName_n]})"
    endRouteEntry="@end_routeentry"
    # Visit the webpage for SUMO standards on Vehicle Type IDs http://sumo-sim.org/wiki/Definition_of_Vehicles,_Vehicle_Types,_and_Routes#Vehicle_Types
	# [Reason 4] Look at the "Reason List" on top for explanations.
    beginVType="@start_vTypeID_({vTypeID,accel,decel,sigma,tau,length,minGap,maxSpeed,speedFactor,speedDev,[R G B],vClass,emissionClass,guiShape,width,imgFile,impatience,laneChangeModel})"
    endVType="@end_vTypeID"
    # Visit the webpage for SUMO standards on Vehicles and their attributes http://sumo-sim.org/wiki/Definition_of_Vehicles,_Vehicle_Types,_and_Routes#Vehicle_Types
    # Find entry as depart and exit as arrival in the official documentation. These terms 'entry' and 'exit' are used for mere simplicity in understanding.
	# [Reason 4] Look at the "Reason List" on top for explanations.
    beginVehID="@start_vehID_({uID,vTypeID,routeID,[R G B],entryTime,entryLane,entryPtPos,entrySpeed,exitLane,exitPos,exitSpeed,line})" 
    endVehID="@end_vehID"
    fileStore   = os.getcwd ()
    baseName = os.path.basename (fileString)
    extName = os.path.splitext (baseName)
    # Decide file extension based on input.
    if extName[1] == '.sncf' or extName[1] == '.SNCF':
        fileText    = fileString
    else:
        fileText = fileString+fileExt
        
    fullPath=fileStore+slashChar+fileText
    fileHandle=open(fileText,'w')
    fileHandle.write(fileType+'\n')
    fileHandle.write(fileNSID+'\n')
    fileHandle.write(fileAuthor+'\n')
    fileHandle.write(fileCreated+'\n')
    # fileHandle.write (fileDocument+'\n') [Reason 1]
    fileHandle.write(beginCoord+'\n')
    # fileHandle.write (addCoord+'\n') [Reason 2]
    fileHandle.write(endCoord+'\n')
    # [Reason 2]
    fileHandle.write(beginElist+'\n')
    fileHandle.write(endElist+'\n')
    fileHandle.write(beginVType+'\n')
    fileHandle.write(endVType+'\n')
    fileHandle.write(beginRouteEntry+'\n')
    fileHandle.write(endRouteEntry+'\n')
    fileHandle.write(beginVehID+'\n')
    fileHandle.write(endVehID+'\n')
    fileHandle.close ()

    print '''[makeSNCF says]: The file %s has been written to location %s.''' %(fileText,fileStore)
    print 'Syncing with filesystem...'
    waitForSync(5)
    print '[makeSNCF says]: Success'

def showSNCF (fileName):
    '''
A function that displays the contents of an SNCF file.
    '''
    print '[showSNCF says:]:'
    try:
        with open (fileName) as fileHandle:
            for allLines in fileHandle:
                print allLines

    except IOError:
        return None
        print '''
[showSNCF says]:
The filename %s does not exist!!
There seems to be a problem with the *.sncf that you are trying to view.
Possible reasons:
[1] : Make sure you have typed in the correct filename of the *.sncf file. The filenames are case sensitive.
[2] : Make sure you have the *.sncf file in your working directory.
        ''' %(fileName)

def addAuthor (fileName,nameAuthor):
    '''
A function used to add information about the author 'nameAuthor' in the sncf 'fileName'.
The function writes/appends the author information in the sncf 'fileName'. Usually 'fileName'
is an empty *.sncf instance, the addxxxxx family of functions adds the desired information
into an empty *.sncf instance. If the author field already exists, it appends the new entry
with the old one, making the new Author entry as the co-author.
    '''
    try:
        with open (fileName,'r') as fileHandler:
            dataInFile = fileHandler.readlines()
            dataInFile[2] = dataInFile[2].strip()
            if "@author" in dataInFile[2]:
                if (len(dataInFile[2]) > 8):
                    dataInFile[2] = dataInFile[2]+','+nameAuthor+printNew()
                    print '[addAuthor says]: Added author information \"%s\" to file %s' %(nameAuthor,fileName)
                    print '[addAuthor suggests]: To view changes try [showSNCF] method.'
                else:
                    dataInFile[2] = dataInFile[2]+nameAuthor+printNew()
                    print '[addAuthor says]: Added author information \"%s\" to file %s' %(nameAuthor,fileName)
                    print '[addAuthor suggests]: To view changes try [showSNCF] method.'

            else:
                print '[addAuthor says]:'
                print 'Ill formed file. Please for 5 seconds as [addAuthor] is reparing the %s file.' %(fileName)
                print 'Repairing now...'
                waitForSync(5)
                print '[addAuthor says]: %s is now ready for further operations.' %(fileName)
    except IOError,WindowsError:
        print '''
[addAuthor says]:
The filename %s does not exist!!
There seems to be a problem with the *.sncf that you are trying to view.
Possible reasons:
[1] : Make sure you have typed in the correct filename of the *.sncf file. The filenames are case sensitive.
[2] : Make sure you have the *.sncf file in your working directory.
        ''' %(fileName)
    try:
        with open (fileName,'w') as fileHandler:
            fileHandler.writelines(dataInFile)
    except IOError,WindowsError:
        print '''
                [addAuthor says]:
                The file %s is about to be wriiten. Please close the file %s and try addAuthor method again.
                ''' %(fileName,fileName)
        return False
    return None


def addDate (fileName,dateCreated):
    '''
A function used to add information about date of creation 'dateCreated' in the sncf 'fileName'.
The function writes/appends date of creation in the sncf 'fileName'.Usually 'fileName'
is an empty *.sncf instance, the addxxxxx family of functions adds the desired information
into an empty *.sncf instance. If the date field already exists, it deletes the previous entry and
assigns a new author information - 'dateCreated'.
    '''
    try:
        with open (fileName,'r') as fileHandler:
            dataInFile = fileHandler.readlines()
            dataInFile [3] = dataInFile[3].strip()
            if "@created" in dataInFile[3]:
                if (len(dataInFile[3]) > 14):
                    print '''[addDate says]:
                    You are trying to modify the date of creation.
                    Not a permissible operation.
                    '''
                    dataInFile[3] = dataInFile[3]+printNew()
                else:
                    if type(dateCreated) == str:
                        if dateCreated == 'Today' or dateCreated == 'T' or dateCreated == 'today' or dateCreated == 't':
                            dataInFile [3] = dataInFile [3]+datetime.date.today().strftime("%Y-%m-%d")+printNew()
                            print '[addDate says]: Success. [addDate] has updated the date of creation to the file %s.' % (fileName)
                        else:
                            print ''' [addDate says]:
                            The permissible values that are accepted would be:
                            Today/T : Returns the current date.
                            today/t   : Returns the current date.
                            [addDate suggests]:
                            Please check the second argument provided to the [addDate] method.
                        '''
                    elif type(dateCreated) == int:
                        ndaysAgo = datetime.date.today() - datetime.timedelta(days = dateCreated)
                        dataInFile [3] = dataInFile[3]+ndaysAgo.strftime ("%Y-%m-%d")+printNew()
                        print '[addDate says]: Success. [addDate] has updated the date of creation to %s on the file %s.' % (dateCreated,fileName)
                    else:
                        print '''[addDate says]:
                        Only integral values accepted.
                        Please check the second argument provided to the [addDate] method.
                        '''
            else:
                print ''' [addDate says]:
                Cannot complete operation on %s. It looks like the file format is corrupted. Please wait for 5
                seconds, as the file is being repaired.
                [addDate suggests]:
                You might want to run the [showSNCF] method to check if something is wrong.
               ''' %(fileName)
                print '''
                [addDate says]:
                Repairing... Please wait.
                '''
                waitForSync(5)
                print 'Done reparing %s' %(fileName)
        with open (fileName,'w') as fileHandler:
            fileHandler.writelines(dataInFile)
    except IOError,WindowsError:
        print '''
                [addDate says]:
                The filename %s does not exist!!
                There seems to be a problem with the *.sncf that you are trying to view.
                Possible reasons:
                [1] : Make sure you have typed in the correct filename of the *.sncf file. The filenames are case sensitive.
                [2] : Make sure you have the *.sncf file in your working directory.
                ''' %(fileName)
    return None

def addNSID(fileName,nodeStartID):
    '''
A function used to add information about node start ID in the sncf 'fileName'.
The function writes/appends the node start ID information in the sncf 'fileName'.
Usually 'fileName' is an empty *.sncf instance, the addxxxxx family of functions adds
the desired information into an empty *.sncf instance. If the NSID field already exists, it deletes the previous entry and assigns a new author information - 'nodeStartID'.
    '''
    try:
        with open (fileName,'r') as fileHandler:
            dataInFile = fileHandler.readlines()
            dataInFile [1] = dataInFile[1].strip()
            if "@nsi" in dataInFile[1]:
                if (type(nodeStartID) == str or type(nodeStartID) == float):
                    print '''
[addNSID says]:Error! Node identification number cannot be a string literal or a floating valued number.
Please provide an integral value.Cannot complete operation.
                    '''
                    dataInFile [1] = dataInFile [1]+printNew()
                else:
                    dataInFile [1] = '@nsi:\n'
                    dataInFile [1] = dataInFile[1].strip()
                    dataInFile [1] = dataInFile [1]+str(nodeStartID)+printNew()
                    print '''
[addNSID says]: Success. [addNSID] has updated the identification number of the starting
node to %s on the file %s.
                    ''' %(nodeStartID,fileName)
            else:
                print '''
[addNSID says]: Error!Cannot complete operation on %s. It looks like the file format is corrupted. Please wait for 5
seconds, as the file is being repaired.
[addDate suggests]: You might want to run the [showSNCF] method to check if something is wrong.
               ''' %(fileName)
                print '''
[addNSID says]: Repairing... Please wait.
                '''
                waitForSync(5)
                print 'Done reparing %s' %(fileName)

        with open (fileName,'w') as fileHandler:
            fileHandler.writelines(dataInFile)
    except IOError,WindowsError:
        print '''
[addNSID says]: The filename %s does not exist!!
There seems to be a problem with the *.sncf that you are trying to view.
Possible reasons:
[1] : Make sure you have typed in the correct filename of the *.sncf file. The filenames are case sensitive.
[2] : Make sure you have the *.sncf file in your working directory.
                ''' %(fileName)
    return None

def showAuthor (fileName):
    '''
A function that shows author information of an *.sncf file.
    '''
    try:
        with open (fileName,'r') as fileHandler:
            dataInFile = fileHandler.readlines()
            lenHeader = len('@author:')
            lenField = len(dataInFile[2])
            if '@author' in dataInFile[2]:
                if lenField == lenHeader+1 or lenField == lenHeader:
                    print '''
[showAuthor says]: The author information is not updated. Unable to display information.
[showAuthor suggests]: Try [addAuthor] method to update author information.
                    '''
                elif lenField < lenHeader:
                    dataInFile[2] = '@author:'
                    print '''
[showAuthor says]: Data in the author field is corrupted. The field will be restored to original state.
'''
                    with open (fileName,'w') as fileHandler:
                        fileHandler.writelines(dataInFile)
                else:
                    infoAuthor = '\n'
                    for itr in range (lenHeader,lenField):
                        infoAuthor = infoAuthor + dataInFile [2][itr]
                    return infoAuthor.strip()
            else:
                print '''
[showAuthor suggests]:
Cannot complete the operation.
Try adding author information to the file %s. The file format is corrupted, and [addAuthor] method will help in
restoring the file format.
''' %(fileName)
    except IOError,WindowsError:
        print '''
[showAuthor says]:
The filename %s does not exist!!
There seems to be a problem with the *.sncf that you are trying to view.
Possible reasons:
[1] : Make sure you have typed in the correct filename of the *.sncf file. The filenames are case sensitive.
[2] : Make sure you have the *.sncf file in your working directory.
        ''' %(fileName)
        return None

def showNSID(fileName):
    '''
A function that displays the identification number of the first node in the SNCF file.
    '''
    try:
        with open (fileName,'r') as fileHandler:
            dataInFile = fileHandler.readlines()
            lenHeader = len ('@nsi:')
            lenField = len (dataInFile[1])
            if '@nsi' in dataInFile[1]:
                if len(dataInFile[1].strip()) == 5:
                       print '''
[showNSID says]: Unable to complete operation. The first node identity number is not updated in the field.
[showNSID suggests]: Try [addNSID] method to add node start identitification number.
               '''
                elif lenField < lenHeader:
                    dataInFile[1] = '@nsi:'
                    print '''
[showNSID says]: Data in the nsi field is corrupted. The field will be restored to original state.
            '''
                    with open(fileName,'w') as fileHandler:
                        fileHandler.writelines(dataInFile)
                else:
                       infoNSID = '\n'
                       for itr in range (lenHeader,lenField):
                           infoNSID = infoNSID + dataInFile[1][itr]
                       return infoNSID.strip()
            else:
                print '''
[showNSID suggests]:
Cannot complete the operation.
Try adding nsi information to the file %s. The file format is corrupted, and [addNSID] method will help in
restoring the file format.
        ''' %(fileName)

    except IOError,WindowsError:
        print '''
[showNSID says]:
The filename %s does not exist!!
There seems to be a problem with the *.sncf that you are trying to view.
Possible reasons:
[1] : Make sure you have typed in the correct filename of the *.sncf file. The filenames are case sensitive.
[2] : Make sure you have the *.sncf file in your working directory.
        ''' %(fileName)
        return None

def showDateOfCreation(fileName):
    '''
A function to display the date of creation of the SNCF file 'fileName'.
    '''
    try:
        with open (fileName,'r') as fileHandler:
            dataInFile = fileHandler.readlines()
            lenHeader = len ('@created:')
            lenField = len (dataInFile[3])
            if '@created' in dataInFile[3]:
                if lenField == lenHeader:
                       print '''
[showDateOfCreation says]: Unable to complete operation. The date of creation is not updated in the field.
[showDateOfCreation suggests]: Try [addDate] method to add date of creation.
               '''
                elif lenField < lenHeader:
                    dataInFile[1] = '@creation:'
                    print '''
[showDateOfCreation says]: Data in the created field is corrupted. The field will be restored to original state.
            '''
                    with open(fileName,'w') as fileHandler:
                        fileHandler.writelines(dataInFile)
                else:
                       infoDOC = '\n'
                       for itr in range (lenHeader,lenField):
                           infoDOC = infoDOC + dataInFile[3][itr]
                       return infoDOC.strip()
            else:
                print '''
[showDateOfCreation suggests]:
Cannot complete the operation.
Try adding nsi information to the file %s. The file format is corrupted, and [addNSID] method will help in
restoring the file format.
        ''' %(fileName)

    except IOError,WindowsError:
        print '''
[showDateOfCreation says]:
The filename %s does not exist!!
There seems to be a problem with the *.sncf that you are trying to view.
Possible reasons:
[1] : Make sure you have typed in the correct filename of the *.sncf file. The filenames are case sensitive.
[2] : Make sure you have the *.sncf file in your working directory.
        ''' %(fileName)
        return None

# TODO: Add functions that returns statistical data of an sncf file.
# DONE, on 20 June 2013
# Statistical data

def getNumberOfNodes(sncfFileName):
    '''
Computes the number of nodes of an sncf file sncfFileName (ver 0.1.4 rev 3).
    '''
    lineBuff = []
    try:
        fileHandler = open (sncfFileName)
        allLines = fileHandler.readlines()
        sizeOf = len(allLines)
        start = 0
        end = 0
        for i in range (0,sizeOf):
            if '@start_coord_node_id_(x,y)' in allLines [i]:
                start = i
        for i in range (0,sizeOf):
            if '@end_coord' in allLines [i]:
                end = i
        for itr in range (start+1,end):
            if allLines [itr] == '\n':
                sncfheaders.readIgnore()
            else:
                lineBuff.append(allLines[itr])
    except IOError,WindowsError:
        print '''
[getNumberOfNodes says]: Cannot open file %s. Make sure the name of the file is valid and it exists.
        ''' %(sncfFileName)
    return len (lineBuff)

def countLines(fileName):
    ''' 
Computes and returns the number of lines of fileName. 
    '''
    try:
        fileHandler = open (fileName)
        allLines = fileHandler.readlines ()
        # Counts the number of empty lines too.
        numberOfLines = len (allLines)
        fileHandler.close ()
    except IOError,WindowsError:
        print '''
[countLines says]: Error. File %s does not exist. Make sure you typed in the right filename and it exists.
        '''%(fileName)
        numberOfLines = None

    return numberOfLines
