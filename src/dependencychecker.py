#!C:\Python27
#!/usr/bin/python
################################### 
# Author:Nitin Deshpande
# Filename:dependencychecker.py
# Date: 2013-09-16
###################################
import os
import time
import makeXML
import setoper
import strutils
import getattribs
import filesanity

def testA(sncfFileName):
	'''
Returns a boolean True if the dependency between vType IDs and Vehicle Types are established.
Returns False, if it does not. 
	'''
	# Test A getattribs.getvType_vTypeID(globFile)=getattribs.getveh_vTypeID(globFile)
	# n_=normalised/without repetitions
	# s_=sorted list
	if filesanity.check_vTypeAttribs(sncfFileName)==True and filesanity.check_routeEntryAttribs(sncfFileName)==True and filesanity.check_vehicleAttribs(sncfFileName)==True:
		listvType=getattribs.getvType_vTypeID(sncfFileName)
		listvehvType=getattribs.getveh_vTypeID(sncfFileName)
		# To eliminate repetitions, perform set union on both the lists.
		n_listvType=setoper.setUnion(listvType,listvType)
		n_listvehvType=setoper.setUnion(listvehvType,listvehvType)
		# To facilitate comparison, sort it alphabetically.
		s_listvType=setoper.listSortAB(n_listvType)
		s_listvehvType=setoper.listSortAB(n_listvehvType)
		if len(s_listvehvType)==len(s_listvType):
			# For every vTypeID there must be equal number of vTypeID in vehicleAttributes. Not only the number of 
			# vTypeIDs must be the same, but, also IDs must be the same to pass testA.
			tValue=True
			for itr in range(0,len(s_listvehvType)):
				if s_listvehvType[itr]==s_listvType[itr]:
					oValue=True
					tValue=oValue and tValue
				else:
					oValue=False
					tValue=oValue and tValue
			return tValue
		else:
			print'''
[testA says]:Error!Illegal dependency check. Check the file %s for proper association of vTypeID of vType attribute with the vTypeID of vehicle attribute.The vTypeID of vehicle attribute must contain the same vTypeID of vType attribute. 
			'''%(sncfFileName)
	else:
		print'''
[testA says]:Error!Illegal dependency check. Check the file %s for proper association of vTypeID of vType attribute with the vTypeID of vehicle attribute.The vTypeID of vehicle attribute must contain the same vTypeID of vType attribute.[testA says]:Error!
		'''%(sncfFileName)
				
def testB(sncfFileName):
	'''
Returns a boolean True if the dependency between routeIDs and Vehicle routeID are established.
Returns False, if it does not. 
	'''
	# Test B getattribs.getroute_routeID(sncfFileName)=getattribs.getveh_routeID(sncfFileName)
	# n_=normalised/without repetitions
	# s_=sorted list
	if filesanity.check_vTypeAttribs(sncfFileName)==True and filesanity.check_routeEntryAttribs(sncfFileName)==True and filesanity.check_vehicleAttribs(sncfFileName)==True:
		listRouteID=getattribs.getroute_routeID(sncfFileName)
		listvehRouteID=getattribs.getveh_routeID(sncfFileName)
		# Set Union operation to eliminate repetitions.
		n_listRouteID=setoper.setUnion(listRouteID,listRouteID)
		n_listvehRouteID=setoper.setUnion(listvehRouteID,listvehRouteID)
		# Sort the list alphabetically
		s_listRouteID=setoper.listSortAB(n_listRouteID)
		s_listvehRouteID=setoper.listSortAB(n_listvehRouteID)
		len_routeID=len(s_listRouteID)
		len_vrouteID=len(s_listvehRouteID)
		if len_routeID>len_vrouteID or len_routeID==len_vrouteID:
			return True
		else:
			return False
	else:
		print'''
[testB says]:Error!Illegal dependency check. Check the file %s for proper association of routeIDs of routeEntry attribute with the routeIDs of vehicle attribute.The routeIDs of vehicle attribute must contain the same routeIDs of vehicle attribute.[testB says]:Error!
		'''%(sncfFileName)
		
def testC(sncfFileName):
	'''
Returns a boolean True if the dependency between edgeIDs in M:*.edg.xml and route edges are established.
Returns False, if it does not.
	'''
	###################################################################################
	# [Test C] edg.xml.edgeList=route.edges
	# Get edge IDs from  sortedList(makeXML.getEdgeIDs) and sortedList(setUnion(getattribs.getvectEdgeNames))
	if filesanity.check_vTypeAttribs(sncfFileName)==True and filesanity.check_routeEntryAttribs(sncfFileName)==True and filesanity.check_vehicleAttribs(sncfFileName)==True:
		listM_edgeIDs=setoper.listSortAB(setoper.setUnion(makeXML.getEdgeIDs(sncfFileName),makeXML.getEdgeIDs(sncfFileName)))
		lenMEdgeIDs=len(listM_edgeIDs)
		listroute_edgeIDs=setoper.listSortAB(setoper.setUnion(getattribs.getvectEdgeNames(sncfFileName),getattribs.getvectEdgeNames(sncfFileName)))
		lenrouteEdgeIDs=len(listroute_edgeIDs)
		if lenMEdgeIDs>lenrouteEdgeIDs or lenMEdgeIDs==lenrouteEdgeIDs:
			return True
		else:
			return False
	else:
		print'''
[testC says]:Error!Illegal dependency check. Check the file %s for proper association of edgeIDs of M:Paradigm *.edg.xml file with the edgeIDs of routeEntry attribute.The edgeIDs of routeEntry attribute must contain the same edgeIDs of M:Paradigm *.edg.xml.[testC says]:Error!
		'''%(sncfFileName)
	
def depChecker(sncfFileName):
	'''
Function that checks the sanity of the sncf file by checking the dependency conditions in the route meta-data.
If the depChecker tests are all passed, then the *.rou.xml files are generated, else aborted.	
	'''
	if filesanity.check_vTypeAttribs(sncfFileName)==True and filesanity.check_routeEntryAttribs(sncfFileName)==True and filesanity.check_vehicleAttribs(sncfFileName)==True:
		if testA(sncfFileName)==True and testB(sncfFileName)==True and testC(sncfFileName)==True:
			return True
		else:
			return False
	else:
		print'''
[depChecker says]:Error!Dependency check failed. Please check the %s file again.
		'''%(sncfFileName)