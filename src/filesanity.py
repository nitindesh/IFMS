#!C:\Python27
#!/usr/bin/python
# Name:filesanity.py
# SUMO:(S)imulation of (U)rban (MO)bility (SUMO)
# Author:Nitin Deshpande
# In compliance with Version 0.1.4 rev 1 of the SNCF Format.
# Fixed getHeaderIndices function with string interning capabilities on 2:15 AM Friday, November 20, 2013
# Fixed the call signature of findHeader(fileName,searchTerm) function to findHeader(searchTerm,sncfFileName) on 2:30 AM Friday, November 20, 2013
# Fixed the findHeader function with string interning capabilities on 3:01 AM Friday, November 20, 2013

import sncfheaders
import os
import makeXML
import getattribs

# def saneCheckValues(fileName):
    # '''
# Checks the sanity of the file fileName. Checks if there is just one comma per entry in the node
# co-ordinate plan file. (A normal *.txt file.)
# 1 denotes sanity and 0 denotes corrupted or ill-formed file.
    # '''
    # fileHandler=open(fileName)
    # readAllLines=fileHandler.readlines ()
    # for itr in readAllLines:
        # if itr.strip().count(',')==1:
            # boolSane=1
        # if itr.strip().count(',')>1 or itr.strip().count(',')<1:
            # boolSane=0
    # fileHandler.close()
    # return boolSane

def findHeader(searchHeader,sncfFileName):
	'''
Function that returns the line number of the header name searchHeader in the file sncfFileName. 
	'''
	try:
		fileHandler=open(sncfFileName,'r')
		allLines=fileHandler.readlines()
		fileHandler.close()
		startLN=0
		for itr in range(0,len(allLines)):
			line_S=allLines[itr]
			line_S=line_S.split('\n')[0]
			lineIntern=intern(line_S)
			headerIntern=intern(searchHeader)
			if lineIntern==headerIntern:
				startLN=itr+1
				break
			else:
				startLN=None
	except:
		print'''
[findHeader says]: Error! The filename is not marked for reading or the file %s does not exist in the location %s.
Check if the location is your working directory and try again.
		'''%(sncfFileName,os.getcwd())
	return startLN
	
def getHeaderIndices(startHeader,endHeader,sncfFileName):
	'''
Function that returns list of the index/line number of the header names beginHeader and endHeader. 
	'''
	fileHandler=open(sncfFileName)
	allLine=fileHandler.readlines()
	lenLines=len(allLine)
	sN=0
	eN=0	
	for itr in range(0,lenLines):
		buffData=allLine[itr]
		buffData=buffData.split('\n')[0]
		lineIntern=intern(buffData)
		dataIntern=intern(startHeader)
		if lineIntern==dataIntern:
			sN=itr
			break
		else:
			sN=None
	for itr in range(0,lenLines):
		buffData=allLine[itr]
		buffData=buffData.split('\n')[0]
		lineIntern=intern(buffData)
		dataIntern=intern(endHeader)
		if lineIntern==dataIntern:
			eN=itr
			break
		else:
			eN=None
	return [sN,eN]
	
def check_vTypeAttribs(sncfFileName):
	'''
Function that checks the number of meta-data present in the vType attributes. Returns True if the number of meta-data
is equal to 18 or returns False otherwise.
	'''
	tValue=True
	for itr in range(0,len(getattribs.getvTypeAttribs(sncfFileName))):
		if len(getattribs.getvTypeAttribs(sncfFileName)[itr])==18:
			oValue=True
			tValue=oValue and tValue
		else:
			oValue=False
			tValue=oValue and tValue
	return tValue

def check_routeEntryAttribs(sncfFileName):
	'''
Function that checks the number of meta-data present in the routeEntry attributes. Returns True if the number of meta-data is equal to 3 or returns False otherwise.
	'''
	tValue=True
	for itr in range(0,len(getattribs.getrouteEntryAttribs(sncfFileName))):
		if len(getattribs.getrouteEntryAttribs(sncfFileName)[itr])==3:
			oValue=True
			tValue=oValue and tValue
		else:
			oValue=False
			tValue=oValue and tValue
	return tValue
	
def check_vehicleAttribs(sncfFileName):
	'''
Function that checks the number of meta-data present in the vehicleAttribs attributes. Returns True if the number of meta-data is equal to 12 or returns False otherwise.
	'''
	tValue=True
	for itr in range(0,len(getattribs.getvehicleAttribs(sncfFileName))):
		if len(getattribs.getvehicleAttribs(sncfFileName)[itr])==12:
			oValue=True
			tValue=oValue and tValue
		else:
			oValue=False
			tValue=oValue and tValue
	return tValue