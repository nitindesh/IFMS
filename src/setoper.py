#!C:\Python27
#!/usr/bin/python
################################### 
# Author:Nitin Deshpande
# Filename:setoper.py
# Date: 2013-09-25
###################################
import os

def setUnion(listOne,listTwo):
	'''
Function that returns the set union of two non empty sets.
	'''
	# check if the arguments are of type lists
	if type(listOne)==list and type(listTwo)==list:
		print'''
[setUnion says]:Done! Union operation performed.
		'''
		return list(set(listOne)|set(listTwo))
	else:
		print'''
[setUnion says]:Error! Either listOne or listTwo is not a list. setUnion accepts lists as the input arguments.
Check the type of the arguments by typing in type(variableName) in the python interactive shell to check the datatype.
		'''
def setIntersect(listOne,listTwo):
	'''
Function that returns the set intersection of two non empty sets.
	'''
	# check if the arguments are of type lists
	if type(listOne)==list and type(listTwo)==list:
		print'''
[setIntersect says]:Done! Intersection operation performed.
		'''
		return list(set(listOne) & set(listTwo))
	else:
		print'''
[setIntersect says]:Error! Either listOne or listTwo is not a list. setIntersect accepts lists as the input arguments.
Check the type of the arguments by typing in type(variableName) in the python interactive shell to check the datatype.
		'''
def setDiffer(listOne,listTwo):
	'''
Function that returns the set difference of two non empty sets.
	'''
	# check if the arguments are of type lists
	if type(listOne)==list and type(listTwo)==list:
		print'''
[setDiffer says]:Done! Set difference performed.
		'''
		return list(set(listOne) - set(listTwo))
	else:
		print'''
[setDiffer says]:Error! Either listOne or listTwo is not a list. setDiffer accepts lists as the input arguments.
Check the type of the arguments by typing in type(variableName) in the python interactive shell to check the datatype.
		'''
def listSortAB(listString):
	'''
Function that returns the set union of two non empty sets.
	'''
	lenList=len(listString)
	# for itr in listString:
	if type(listString)==list:
		print'''
[listSortAB says]:Done! List sorted alphabetically.
		'''
		sortBuff=[]
		listString.sort()
		for itr in listString:
			sortBuff.append(itr)
		return sortBuff
	else:
		print'''
[listSortAB says]:Error! The input argument is not a list.
Check the type of the arguments by typing in type(variableName) in the python interactive shell to check the datatype.
		'''
def listAppend(listOne,listTwo):
	'''
Function that returns the appended list.
	'''
	# check if the arguments are of type lists
	if type(listOne)==list and type(listTwo)==list:
		print'''
[setAppend says]:Done! Lists appended.
		'''
		return listOne+listTwo
	else:
		print'''
[setAppend says]:Error! Either listOne or listTwo is not a list. setAppend accepts lists as the input arguments.
Check the type of the arguments by typing in type(variableName) in the python interactive shell to check the datatype.
		'''
def areListSame(listA,listB):
	'''
Function that returns True if every element of listA is same as every element of listB.
	'''
	tValue=True
	if len(listA)==len(listB):
		for itr in range(0,len(listA)):
			if listA[itr]==listB[itr]:
				oValue=True
				tValue=tValue and oValue
			else:
				oValue=False
				tValue=tValue and oValue
		return tValue
	else:
		print'''
[areListSame says]: The length of both lists %s and %s should be the same. Check the length of lists by typing in
len(%s) and len(%s) in an interactive python shell and try again.
		'''%(listA,listB,listA,listB)
			