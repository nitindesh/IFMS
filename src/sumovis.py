#!C:\Python27
#!/usr/bin/python
# Name:sumovis.py
# SUMO:(S)imulation of (U)rban (MO)bility (SUMO)
# Author:Nitin Deshpande
# In compliance with Version 0.1.4 rev 3 of the SNCF Format.

import globalasp
import subprocess
import os
import time
import sncfheaders

def viewVtk(cfgFile,opvtkFile):
    '''
Function that examines the cfg file cfgFile of a simulation and then writes the details in a VTK file opvtkFile.
    '''
    prevWD=os.getcwd()
    relPath=globalasp.getPPath('SUMO') #Gets the relative path of SUMO binary
    cfgFilePath=prevWD+'\\'+cfgFile
    dirString=time.strftime('%H_%M_%S_%d_%m_%y')
    createDir=os.makedirs(dirString)
    print'''
[viewVtk says]:Creating directory %s at location %s.
    '''%(dirString,prevWD)
    newPath=os.getcwd()+'\\'+dirString
    print'''
[viewVtk says]:Changing into directory %s.
    '''%(newPath)
    os.chdir(newPath)
    if os.path.exists(cfgFilePath):
        vtkFlag=' --vtk-output'
        #Putting everything together ---- (V)
        rollOut=relPath+' '+'-c '+cfgFilePath+' '+vtkFlag+' '+opvtkFile
        #Executing the command (V)
        subprocess.check_output(rollOut)
        print '''
[viewVtk says]:Working ... Please wait.The output file %s has been written to %s. 
        '''%(opvtkFile,newPath)
        print'''
[viewVtk says]:Done!Changing the working directory to %s.Syncing....wait a moment.
        '''%(prevWD)
        os.chdir(prevWD)
        sncfheaders.waitForSync(3)
    else:
        print'''
[viewVtk says]:Error!Check if the file %s exists in the working directory %s.If the file exists,check if you have enough permissions to execute the
file.
        '''%(opvtkFile,newPath)
    
def viewFullOutput(cfgFile,opFile):
    '''
Function that examines the cfg file cfgFile of a simulation and then writes the full output details in a file opFile.
    '''
    if os.path.exists(cfgFile):
        relPath=globalasp.getPPath('SUMO')#Gets the relative path of SUMO binary
        foFlag='--full-output'
        #Putting everything together ---- (V)
        rollOut=relPath+' '+'-c '+cfgFile+' '+foFlag+' '+opFile
        #Executing the command (V)
        subprocess.check_output(rollOut)
        print '''
[viewFullOutput says]:Working ... Please wait.The output file %s has been written to %s. 
        '''%(opFile,os.getcwd())
    else:
        print'''
[viewFullOutput says]:Error!Check if the file %s exists in the working directory %s.If the file exists,check if you have enough permissions to execute the
file.
        '''%(cfgFile,os.getcwd())

def viewFCD(cfgFile,netFile,settingFile,routeFile,opFile):
    '''
Function to output the floating car data (FCD) using a configuration file 'cfgFile',settings file 'settingFile' and route file 'routeFile'.
    '''
    
    if os.path.exists(cfgFile) and os.path.exists(netFile) and os.path.exists(settingFile) and os.path.exists(routeFile):
        relPath=globalasp.getPPath('SUMO')#Gets the relative path of SUMO binary
        rollOut=relPath+' -c '+cfgFile+' -n '+netFile+' --fcd-output '+opFile
        subprocess.check_output(rollOut)
        print '''
[viewFCD says]:Working ... Please wait.The output file %s has been written to %s. 
        '''%(opFile,os.getcwd())
    else:
        print'''
[viewFCD says]:Error!Check if the file(s) %s,%s,%s and %s exists in the working directory %s.If the file exists,check if you have enough permissions to execute the
file.
        '''%(cfgFile,netFile,settingFile,routeFile,os.getcwd())

def viewRawDump(cfgFile,netFile,settingFile,routeFile,opdumpFile):
    '''
Function to output the raw dump data using a configuration file 'cfgFile',settings file 'settingFile' and route file 'routeFile'.
    '''
    
    if os.path.exists(cfgFile) and os.path.exists(netFile) and os.path.exists(settingFile) and os.path.exists(routeFile):
        relPath=globalasp.getPPath('SUMO')#Gets the relative path of SUMO binary
        rollOut=relPath+' -c '+cfgFile+' -n '+netFile+' --netstate-dump '+opdumpFile
        subprocess.check_output(rollOut)
        print '''
[viewRawDump says]:Working ... Please wait.The output file %s has been written to %s. 
        '''%(opdumpFile,os.getcwd())
    else:
        print'''
[viewRawDump says]:Error!Check if the file(s) %s,%s,%s and %s exists in the working directory %s.If the file exists,check if you have enough permissions to execute the
file.
        '''%(cfgFile,netFile,settingFile,routeFile,os.getcwd())
        
def viewSummary(cfgFile,netFile,settingFile,routeFile,opsummaryFile):
    '''
Function to output the summary data using a configuration file 'cfgFile',settings file 'settingFile' and route file 'routeFile'.
    '''
    
    if os.path.exists(cfgFile) and os.path.exists(netFile) and os.path.exists(settingFile) and os.path.exists(routeFile):
        relPath=globalasp.getPPath('SUMO')#Gets the relative path of SUMO binary
        rollOut=relPath+' -c '+cfgFile+' -n '+netFile+' --summary '+opsummaryFile
        subprocess.check_output(rollOut)
        print '''
[viewSummary says]:Working ... Please wait.The output file %s has been written to %s. 
        '''%(opsummaryFile,os.getcwd())
    else:
        print'''
[viewSummary says]:Error!Check if the file(s) %s,%s,%s and %s exists in the working directory %s.If the file exists,check if you have enough permissions to execute the
file.
        '''%(cfgFile,netFile,settingFile,routeFile,os.getcwd())
        
def convert2CSV(fulloutputFile):
    '''
Function that converts a FULL dump output file into a CSV file.
    '''
    homeDir=os.getcwd()
    scriptPath='xml2csv.py' #Path where script resides
    outputPath=fulloutputFile #Path where output file resides
    fileName='convert2csv.bat'
    csvFile=fulloutputFile+'.csv'
    batFilePath=homeDir+'\\'+fileName
    if os.path.exists(scriptPath) and os.path.exists(outputPath):
        print'''
[convert2CSV says]:Found xml2csv.py and the full dump output file %s.
        '''%(fulloutputFile)
        batString='python '+scriptPath+' '+outputPath
        print'''
[convert2CSV says]:Converting the output file %s into CSV file.A blank black screen will appear now.Be patient,it is churning all the dump into meaningful
data.
        '''%(fulloutputFile)
        subprocess.check_output(batString)
        print'''
[convert2CSV says]:Syncing all files. Wait a moment.
        '''
        sncfheaders.waitForSync(5)
        print'''
[convert2CSV says]:Conversion completed! The CSV file %s has been written to location %s.
        '''%(csvFile,os.getcwd())
    else:
        print'''
[convert2CSV says]:Error! Either the script xml2csv.py could not be located or a valid full output dump is missing.Check if xml2csv.py and the output dump
file %s exists in the working directory %s.        
        '''%(fulloutputFile,os.getcwd())
