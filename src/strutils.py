#!C:\Python27
#!/usr/bin/python
# Name:strutils.py
# SUMO:(S)imulation of (U)rban (MO)bility (SUMO)
# Author:Nitin Deshpande

import os
import sys
import sncfheaders
import filesanity

def whatsInBetween(charFirst,charSecond,inpString):
    '''
Displays the substring of the input inpString between charFirst and charSecond. 
    '''
    try:
        foundFirst=0
        foundSecond=0
        strLen = len(inpString)
        for itr in range(0,strLen):
            if inpString[itr]==charFirst:
                foundFirst=itr
                break
            else:
                pass
        for itr in range(foundFirst+1,strLen):
            if inpString[itr]==charSecond:
                foundSecond = itr
                break
            else:
                pass
        #Find relative distance between the first and the second character.    
        diffBetIndex = foundSecond-foundFirst
        #No data contained in between the first and second character.
        if diffBetIndex==0 or diffBetIndex == 1:
            interString=None
        #Second Character Index lesser than the first character's
        elif diffBetIndex<0:
            interString=inpString[foundSecond+1]
            for itr in range(foundSecond+2,foundFirst):
                interString=interString+inpString[itr]
        #if firstIndex is less than finalindex
        else:
            interString=inpString[foundFirst+1]
            for itr in range(foundFirst+2,foundSecond):
                interString=interString+inpString[itr]
        return interString
    except TypeError:
        sys.stderr.write ('[whatsInBetween says]:whatsInBetween takes exactly three parameters as input.Make sure the function call is supplied with the corr        ect number of parameters')

def betweenHeaders(startHeaderName,endHeaderName,fileName):
    '''
Returns the lines in between start and end header(s).
    '''
    fileHandler=open(fileName)
    allLines=fileHandler.readlines()
    # Count the number of lines in the file
    noLines=sncfheaders.countLines(fileName)
    # Computer the indices of the search-terms.
    sInd=filesanity.findHeader(startHeaderName,fileName)
    eInd=filesanity.findHeader(endHeaderName,fileName)
    # print sInd,eInd Uncomment for diagnostics
    # Initiate linebuffer
    lineBuff=[]
    # Loop through the lines and store the contents in the buffer.
    for itr in range(sInd,eInd-1):
        if allLines[itr]=='\n':
            sncfheaders.readIgnore()
        else:
            lineBuff.append(allLines[itr])
    return lineBuff
		
def isListHomogeneous(dataObject,dataType):
	'''
Function that returns True if the list is homogeneous.
	'''
	retResult=True
	if type(dataObject)==list:
		for itr in dataObject:
			if (type(itr) is dataType)==True:
				retResult=True
			if (type(itr) is dataType)==False:
				retResult=False
				break
		return retResult
	else:
		print'''
[isListHomogeneous says]:Error! The input data \"%s\" is not a list. The input argument must be a list.
Check the type of the arguments by typing in type(variableName) in the python interactive shell to check the datatype. 
		'''%(dataObject)