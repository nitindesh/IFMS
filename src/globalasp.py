#!C:\Python27
#!/usr/bin/python

# Name:globalasp.py
# SUMO:(S)imulation of (U)rban (MO)bility (SUMO)
# In compliance with Version 0.1.4 rev 1 of the SNCF Format.
# Author:Nitin Deshpande
# Created: 02 October 2013

import os
import sys
import subprocess
import sncfheaders
import platform
    
def returnEnvDets(sumoWorkDir):
    '''
Function that returns the bin directory of SUMO. Returns the path if it exists, None if it does not.
    '''
    if os.path.exists(sumoWorkDir)==True:
        return sumoWorkDir
    else:
        return None

def checkEnvDets(sumoWorkDir):
    '''
Returns true if the SUMO path exists, else returns false.
    '''
    if os.path.exists(sumoWorkDir)==True:
        return True
    else:
        return False

def writeSUMOBin(sumoBinLocation):
    '''
Writes the path of sumo bin directory into global file binFile.edf.
    '''
    fileName='binFile.edf'
    currDir=os.getcwd()
    if checkEnvDets(sumoBinLocation)==True:
        fileHandler=open(fileName,'w')
        fileHandler.write(sumoBinLocation)
        fileHandler.close()
        print'''
[loadSUMOBin says]: The file \"%s\" has been written to location %s.
        '''%(fileName,currDir)
    else:
        print'''
[loadSUMOBin says]: Error! The file has not been written, because the path for the bin directory does not exist. Check
the path and try again.
            '''
def loadSUMOBin():
    '''
Loads the absolute path of the bin directory.
    '''
    fileName='binFile.edf'
    currDir=os.getcwd()
    compPath=currDir+'\\'+fileName
    if checkEnvDets(compPath) == True:
        fileHandle=open(compPath)
        allLines=fileHandle.readlines()
        return allLines[0]
    else:
        print'''
[loadSUMOBin says]: Error! binFile.edf was not found in the location %s. Check if the file exists in the current directory.
        '''%(currDir)
        
# Call changed on 21 October 2013
def envLocation(macroCode):
    '''
Writes the location of installation(fileString) of SUMO, and loads the location everytime the framework initiates.
    '''
    fileOpen='envdetails.edf'
    defaultLocation=os.getcwd() 
    processCode=PPath(macroCode)
    try:
        fileHandler=open(fileOpen,'a')
        retResult=fileHandler.write(macroCode+','+loadSUMOBin()+'\\'+processCode+'\n')
        fileHandler.close() 
        print '''
[envLocation says]:The definition %s has been written to location %s.
        '''%(fileOpen,defaultLocation)
    except IOError:
        print '''
[envLocation says]:Error. Check the privileges of the directory that you are trying to write into. Or check administrative privileges associated with the user before writing to the file. Definitions has not been written.
        '''

def helpPPath():
    '''
helpPPath returns macros for SUMO utility applications.
    '''
    macroCode = '''
    MACROCODE   | Process Executed
    ------------------------------
    ACTIVGEN    : activitygen.exe
    DFROUTE     : dfrouter.exe
    DUAROUT     : duarouter.exe
    JTROUTE     : jtrouter.exe
    NETCONV     : netconvert.exe
    NETGEN      : netgenerate.exe
    OD2TRIP     : od2trips.exe
    POLYCON     : polyconvert.exe
    SUMOGUI     : sumo-gui.exe
    SUMO        : sumo.exe
    TRAC        : TraciTestClient.exe
    '''
    return macroCode

def PPath(macroCode):
    '''
Function that returns the utility process that is associated with a Macrocode.
    '''
    pList = ['activitygen.exe','dfrouter.exe','duarouter.exe','jtrouter.exe','netconvert.exe','netgenerate.exe','od2trips.exe','polyconvert.exe','sumo-gui.exe','sumo.exe','TraciTestClient.exe']
    codeList = ['ACTIVGEN','DFROUTE','DUAROUT','JTROUTE','NETCONV','NETGEN','OD2TRIP','POLYCON','SUMOGUI','SUMO','TRAC']
    retPresent = False
    for itr in range (0,len (pList)):
        # If macroCode exists in the codeList, it returns the index to 'foundIndex' mem location.
        if codeList[itr] == macroCode:
            foundIndex = itr
            retPresent = True
            break
        else:
            retPresent = False
    if retPresent == True:
        # Use the foundIndex information to associate the pList to process name.
        pName = pList [foundIndex]
        return pName
    else:
        print '''
[PPath says]:Error. The macro code is incorrect. Please refer to helpPPath() function for more information on macro 
codes.  '''
        pName = 'Error'
        return pName

def getPPath (macroCode):
    '''
Function that fetches the relative path for a given macrocode.
    '''
    fullPath=None
    try:
        fileHandler = open ('envdetails.edf')
        allLines = fileHandler.readlines()
        nLines = sncfheaders.countLines ('envdetails.edf')
        for itr in range(0,nLines):
            if allLines [itr].split(',')[0] == macroCode:
                fullPath = allLines [itr].split(',')[1].split('\n')[0]
                break
            else:
                pass
                fullPath=None
            # Error: Return None when error arises.
    except:
        print'''
[getPPath says]: Could not locate the file \"envdetails.edf\". Check for the existence of the file and try again.
    '''
    return fullPath
