#!C:\Python27
#!/usr/bin/python
# Name:makeCFG.py
# SUMO:(S)imulation of (U)rban (MO)bility (SUMO)
# Author:Nitin Deshpande
import sncfheaders
import os

def makeViewSetting(xValue,yValue,zoomValue,delay,outputSettingFile):
	'''
Function that writes the settings file with viewport values to set the camera position and we used delay values to set the delay between each step of the simulation in ms.
	'''
	viewSettingBegin='<viewsettings>'+sncfheaders.printNew()
	viewPortY='    <viewport y=\"'
	viewPortX='\" x=\"'
	viewPortZoom='\" zoom=\"'
	viewPortEnd='\" />'+sncfheaders.printNew()
	delayValue='    <delay value=\"'
	delayEnd='\" />'+sncfheaders.printNew()
	viewSettingEnd='</viewsettings>'+sncfheaders.printNew()
	try:
		fileHandler=open(outputSettingFile,'w')
		viewsettingsWrite=viewSettingBegin+viewPortY+str(yValue)+viewPortX+str(xValue)+viewPortZoom+str(zoomValue)+viewPortEnd+delayValue+str(delay)+delayEnd+viewSettingEnd
		fileHandler.write(viewsettingsWrite)
		fileHandler.close()
		print'''
[makeViewSetting says]:Done! The file %s has been written to the location %s.
		'''%(outputSettingFile,os.getcwd())
	except IOError,WindowsError:
		print'''
[makeViewSetting says]:Error! Could not write the view settings file %s. Check if the user has administrative privileges and try again.  
		'''%(outputSettingFile)
		
def makeCFG(networkFile,routeXMLFile,guiSettingFile,timeBeginVal,timeEndVal,cfgFileName):
	'''
Function that writes the configuration(*.sumo.cfg) file from input network (*.net.xml),route(*.rou.xml) and settings(*.settings.xml) files.
	'''
	if os.path.exists(networkFile)==True and os.path.exists(routeXMLFile)==True and os.path.exists(guiSettingFile)==True:
		netBase=os.path.split(networkFile)[1]
		routeBase=os.path.split(routeXMLFile)[1]
		guiBase=os.path.split(guiSettingFile)[1]
		endTag='\"/>'+sncfheaders.printNew()
		cfgBegin='<configuration>'+sncfheaders.printNew()
		cfgEnd='</configuration>'+sncfheaders.printNew()
		inpBegin='		<input>'+sncfheaders.printNew()
		inpEnd='		</input>'+sncfheaders.printNew()
		netFile='			<net-file value=\"'
		routeFile='			<route-files value=\"'
		guiFile='			<gui-settings-file value=\"'
		timeBegin='		<time>'+sncfheaders.printNew()
		timeEnd='		</time>'+sncfheaders.printNew()
		beginValue='			<begin value=\"'
		endValue='			<end value=\"'
		cfgWrite=cfgBegin+inpBegin+netFile+str(netBase)+endTag+routeFile+str(routeBase)+endTag+guiFile+str(guiBase)+endTag+inpEnd+timeBegin+beginValue+str(timeBeginVal)+endTag+endValue+str(timeEndVal)+endTag+timeEnd+cfgEnd
		try:
			fileHandler=open(cfgFileName,'w')
			fileHandler.write(cfgWrite)
			fileHandler.close()
			print'''
[makeCFG says]:Done! The configuration file %s has been written to the location %s. 
			'''%(cfgFileName,os.getcwd())
		except IOError,WindowsError:
			print'''
[makeCFG says]: Error creating the file %s. Check if the user has the appropriate privileges to create the file.
			'''%(cfgFileName)
	else:
		print'''
[makeCFG says]:Error! One or more files required to generate the configuration files are missing or may not exist in the working directory.
Possible causes:
1. Check if the network file %s exists.
2. Check if the route file %s exists.
3. Check if the gui settings %s file exists.
Check if the files are located in the working directory and try again.
		'''%(networkFile,routeXMLFile,guiSettingFile)
