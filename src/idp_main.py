import os
import custommath
import dependencychecker
import filesanity
import getattribs
import globalasp
import makeCFG
import makeXML
import routegeometry
import setoper
import sncfheaders
import strutils
import PySUMO
import sumovis
import subprocess

# Export path to home directories
os.chdir('D:\IFMS\src')
# Create the sncf required
sncfName='idp_main_alternate.sncf'
#sncfName='idp_final_main.sncf'
# Create the nodes,edges and net XML files.
nodFile='idp_finalNodes.nod.xml'
edgFile='idp_finalEdges.edg.xml'
##netFile='idp_finalEdges.net.xml'
netFile='idp_finalEdges_mod.net.xml'
##rouFile='idp_distribution.rou.xml' # Alternate route, two routes with probability 0.25 each and one route with probability 0.50
rouFile='idp_cfm_distribution.rou.xml' # Alternate route with CFM-PWagner2009
#rouFile='idp_finalRoute.rou.xml'
settingFile='idp_finalSettings.settings.xml'
cfgFile='idp_final.sumocfg'
fullopDump='fullDump'
makeXML.writeCoordinates(sncfName,nodFile)
makeXML.writeEdgeList(sncfName,edgFile)
makeXML.generateNetwork(nodFile,edgFile,netFile)
#################
##makeXML.writeRoute(sncfName,rouFile) #Uncomment if needed!
#################
#Uncomment the next four lines to simulate
makeCFG.makeViewSetting(0.0,1000.0,1,10000,settingFile)
makeCFG.makeCFG(netFile,rouFile,settingFile,0.0,1000,cfgFile)
print 'Making use of %s file'%(sncfName)
print 'Making use of distributions with CFM-PWagner2009 with impatience factors'
print 'Making use of %s to simulate.'%(rouFile)
PySUMO.sumoGUISimulate(cfgFile)
print 'Visualising simulation...'
#For VTK
#sumovis.viewVtk(cfgFile,'emission_Output')
#############
#For Full Output;uncomment the next two lines
sumovis.viewFullOutput(cfgFile,fullopDump)
sumovis.convert2CSV(fullopDump)
##sumovis.viewFCD(cfgFile,netFile,settingFile,rouFile,'fcdOutput.xml')
##sumovis.viewSummary(cfgFile,netFile,settingFile,rouFile,'rawSummary.xml')
##sumovis.convert2CSV('rawSummary.xml')
