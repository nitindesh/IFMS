#!C:\Python27
#!/usr/bin/python
# Name:routegeometry.py
# SUMO:(S)imulation of (U)rban (MO)bility (SUMO)
# Author:Nitin Deshpande
# A module that generates the coordinates for a particular 2d geometrical figure.
 
import math
import sncfheaders
import strutils
import os
import filesanity
import custommath

def drawCircle(radiusVal, originX, originY, theta):
	'''
Function that returns the points on the circumference of a circle of a certain radius value radiusVal at origin originX,originY with initial angle angle theta.
	'''
	X=[]
	Y=[]
	for itr in custommath.floatrange(theta,370,10):
		X.append(originX+radiusVal*math.sin(itr*math.pi/180))		
		Y.append(originY+radiusVal*-math.cos(itr*math.pi/180))
	return [X,Y]
	
def getColLen(coordinateAxis):
	'''
Function that returns the number of columns in the coordinate axis matrix.
Where coordinateAxis=drawCircle(radiusVal, originX, originY, theta)
	'''
	for i in range(0,len(coordinateAxis)):
		lCol=len(coordinateAxis[i])
	return lCol

def getRowLen(coordinateAxis):
	'''
Function that returns the number of rows in the coordinate axis matrix.
Where coordinateAxis=drawCircle(radiusVal, originX, originY, theta)
	'''
	return len(coordinateAxis)

def coordListCircle(radiusVal,originX,originY,theta):
	'''
Function that returns the list of coordinate in a comma separated format.
	'''
	coOrdList=[]
	X,Y=drawCircle(radiusVal,originX,originY,theta)
	for i in range(0,len(X)):
		coOrdList.append(str(X[i])+','+str(Y[i])+'\n')
	return coOrdList

def writeCircleCoordinates(listCoordCircle,sncfFileName):
	'''
Function that writes the list of coordinates of circumference of the circle to the sncf file sncfFileName.
listCoordCircle=coordListCircle(radiusVal,originX,originY,theta)
	'''
	if os.path.exists(sncfFileName)==True:
		fileHandle=open(sncfFileName,'r')
		allLines=fileHandle.readlines()
		fileHandle.close()
		print'''
[writeCircleCoordinates says]:Begin writing circle coordinates. 
		'''
		S,E=filesanity.getHeaderIndices('@start_coord_node_id_(x,y)','@end_coord',sncfFileName)
		if S==None or E==None:
			print'''
[writeCircleCoordinates says]: Error! The coordinates have not been written to the file %s. The file might not be a sncf file. Check the file %s and try again.
			'''%(sncfFileName,sncfFileName)
		else:
			allLines[(S+1):E]=listCoordCircle
			fileHandler=open(sncfFileName,'w')
			for itr in range(0,len(allLines)):
				fileHandler.write(allLines[itr])
			fileHandler.close()
			print'''
[writeCircleCoordinates says]:Done! The coordinates of a circular route has been to the file %s in the location %s.
			'''%(sncfFileName,os.getcwd())
	else:
		print'''
[writeCircleCoordinates says]:Error! The coordinates have not been written to the file %s.
Possible Cause(s):
[1] The sncf file %s does not exist.
[2] The sncf file %s does not have write permissions.
		'''%(sncfFileName,sncfFileName,sncfFileName)
	return [S,E]
	
def drawSquare(hVal,kVal,lengthVal):
	'''
Function that draws a square from the origin hVal,kVal of side lengthVal.
	'''
	finalList=[]
	if lengthVal==0.0 or lengthVal==None:
		lengthVal=1.0
	xVal=[hVal,hVal+lengthVal,hVal+lengthVal,hVal]
	yVal=[kVal,kVal,kVal+lengthVal,kVal+lengthVal]
	return[xVal,yVal]

def coordListSquare(hVal,kVal,lengthVal):
	'''
Function that returns the list of coordinates in a comma separated format.
	'''
	coOrdList=[]
	X,Y=drawSquare(hVal,kVal,lengthVal)
	for i in range(0,len(X)):
		coOrdList.append(str(X[i])+','+str(Y[i])+'\n')
	return coOrdList
	
def writeSquareCoordinates(listCoordSquare,sncfFileName):
	'''
Function that writes the coordinates of a square to the sncf file sncfFileName.
Where listCoordSquare=coordListSquare(hVal,kVal,lengthVal).
	'''
	if os.path.exists(sncfFileName)==True:
		fileHandle=open(sncfFileName,'r')
		allLines=fileHandle.readlines()
		fileHandle.close()
		print'''
[writeSquareCoordinates says]:Begin writing square coordinates. 
		'''
		S,E=filesanity.getHeaderIndices('@start_coord_node_id_(x,y)','@end_coord',sncfFileName)
		if S==None or E==None:
			print'''
[writeSquareCoordinates says]:Error!The coordinates have not been written to the file %s. The file might not be a sncf file. Check the file %s and try again.
			'''%(sncfFileName,sncfFileName)
		else:
			allLines[(S+1):E]=listCoordSquare
			fileHandler=open(sncfFileName,'w')
			for itr in range(0,len(allLines)):
				fileHandler.write(allLines[itr])
			fileHandler.close()
			print'''
[writeSquareCoordinates says]:Done! The coordinates of the route has been to the file %s in the location %s.
			'''%(sncfFileName,os.getcwd())
	else:
		print'''
[writeSquareCoordinates says]:Error! The coordinates have not been written to the file %s.
Possible Cause(s):
[1] The sncf file %s does not exist.
[2] The sncf file %s does not have write permissions.
		'''%(sncfFileName,sncfFileName,sncfFileName)