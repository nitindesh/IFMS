#!C:\Python27
#!/usr/bin/python
# Author:Nitin Deshpande
# Name:PySUMO.py

import globalasp
import sncfheaders
import os
import subprocess

def sumoSimulate(cfgFile):
	'''
Function that simulates the configuration file without the GUI support.
	'''
	if os.path.exists(cfgFile):
		sumoPath=globalasp.getPPath('SUMO')
		rollOut=sumoPath+' -c '+cfgFile
		subprocess.check_output(rollOut)
		print'''
[sumoSimulate says]: Using SUMO CLI.Working ... Please wait.
		'''
	else:
		print'''
[sumoSimulate says]: Error locating the configuration file %s! Check if the configuration file %s exists.
		'''%(cfgFile,cfgFile)

def sumoGUISimulate(cfgFile):
	'''
Function that simulates the configuration file with the gui support.
	'''
	if os.path.exists(cfgFile):
		sumoPath=globalasp.getPPath('SUMOGUI')
		rollOut=sumoPath+' -c '+cfgFile
		print'''
[sumoGUISimulate says]: Using SUMO GUI:Working ... Please wait.
		'''
		subprocess.check_output(rollOut)
	else:
		print'''
[sumoGUISimulate says]: Error locating the configuration file %s! Check if the configuration file %s exists.
		'''%(cfgFile,cfgFile)
