#!C:\Python27
#!/usr/bin/python
# Name:custommath.py
# SUMO:(S)imulation of (U)rban (MO)bility (SUMO)
# Author:Nitin Deshpande

def floatrange(startVal, endVal=None, stepVal=None):
        '''
Function that returns a list of floating point numbers from startVal to endVal with float increment stepVal.
        '''
        if endVal == None:
                endVal = startVal + 0.0
                startVal = 0.0
        if stepVal == None:
                stepVal = 1.0
        fList = []
        while 1:
                nextVal=startVal + len(fList) * stepVal
                if stepVal > 0 and nextVal >= endVal:
                        break
                elif stepVal < 0 and nextVal <= endVal:
                        break
                fList.append(nextVal)
        return fList
