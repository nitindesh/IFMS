#!C:\Python27
#!/usr/bin/python

# Name:makeXML.py
# SUMO:(S)imulation of (U)rban (MO)bility (SUMO)
# In compliance with Version 0.1.4 rev 1 of the SNCF Format.
# Author :Nitin Deshpande
# Created:01 May 2013 Changed: 26 September 2013,26 October 2013,01 November 2013,
# Changed: 20 January 2014,13 February 2014,20 March 2014,16 April 2014

import os
import sncfheaders
import filesanity
import sys
import subprocess
import globalasp
import time
import dependencychecker
import strutils
import getattribs

def getCoordinates (fileName):
        '''
Function that writes the co-ordinate date to a write buffer. 
        '''
        basName = os.path.basename (fileName)
        extName = basName.split ('.')
        retCoords = []
        if extName [1] == 'sncf' or extName == 'SNCF':
                try:
                        fileHandler=open(fileName)
                        allLines=fileHandler.readlines()
                        numLines = sncfheaders.countLines(fileName)
                        strtNum = filesanity.findHeader('@start_coord_node_id_(x,y)',fileName)# Returns the <line_number+1>
                        endNum = filesanity.findHeader('@end_coord',fileName)
                        # The reason why the interpreter prints the '@end_sncf' line is because, there is a \n character (new line) in the end.
                        # So, endNum-1 should do the trick.
                        for itr in range (strtNum,endNum-1):
                            if allLines [itr] == '\n':
                                sncfheaders.readIgnore()
                            else:
                                retCoords.append(allLines[itr])
                        return retCoords
                except IOError,WindowsError:
                        print 'Error. Filename %s does not exist. Please check the name of the file and try again.'%(fileName)
        else:
                sys.stderr.write ('Error. The routines are performed on *.sncf files alone. Please make sure you use *.sncf files alone.')
                print 'You are using the filename %s.'%(fileName)

def getNodIDs(inp_fileName):
        '''
Function that returns the node id's of the co-ordinates as a list.
        '''
        nsidData=[]
        getNSID=sncfheaders.showNSID(inp_fileName)
        startNSID=int(getNSID)
        numOfNodes=sncfheaders.getNumberOfNodes(inp_fileName)
        for itr in range(0,numOfNodes):
                nsidData.append(itr+startNSID)
        return nsidData

def writeCoordinates (inp_fileName,out_fileName):
        '''
A follow up routine for getCoordinates() method. Reads the co-ordinates from sncf file inp_fileName Writes the co-ordinates to an XML file
out_fileName, that contains node information.
        '''
        # Since sncfheaders.showNSID() returns the NSID in string literal, it would be wise to convert it to integer to able to operate on it.
        # An empty list that makes space for data to write to XML files
        xmlBuff=[]
        # Initial setup
        nsidData=[] # Container for rest of NSID's
        getNSID=sncfheaders.showNSID(inp_fileName)
        startNSID=int(getNSID)
        # Get n number of nodes
        numOfNodes=sncfheaders.getNumberOfNodes(inp_fileName)
        # Append data into nsidData
                # Has NSIDs for the rest of the nodes.
        for itr in range(0,numOfNodes):
                nsidData.append(itr+startNSID)
        # Store the data of coordinates in coordData
        coordData=getCoordinates(inp_fileName)
        ##       Structure design of XML and replace variables with XML data.
        ##       XML initial tags
        nL = sncfheaders.printNew()
        xmlBegin='''<?xml version="1.0" encoding="UTF-8"?>
<nodes xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:noNamespaceSchemaLocation="http://sumo.sf.net/xsd/nodes_file.xsd">'''+sncfheaders.printNew()
        xmlEnd='</nodes>'
        xmlLineBegin='	<node id=\"'
        xmlLineEnd='/>'
        xmlX='x=\"'
        xmlY='y=\"'
        ## For the fact that 'number' of nodes equal the number of NSIDs, generate the exact 'number' of intermediate XML tags.
        fileHandle=open(out_fileName,'w')
        fileHandle.write(xmlBegin)
        for itr in range(0,numOfNodes):
                xmlLine=xmlLineBegin+str (nsidData[itr])+'\"'+' '+xmlX+str(coordData[itr].split(',')[0])+'\"'+' '+xmlY+str(coordData[itr].split(',')[1].split('\n')[0])+'\"'+' '+xmlLineEnd+nL 
                fileHandle.write(xmlLine)
                xmlBuff.append(xmlLine)
                ## Keep the line below for diagnostics!
        ## print xmlBuff 
        fileHandle.write(xmlEnd)
        fileHandle.close()
        baseName=os.path.basename (out_fileName)
        extName=os.path.splitext(baseName)[1]
        if extName=='.xml':
                print'''
[writeCoordinates says]: The file %s has been written to the location %s.
''' %(out_fileName,os.getcwd())
        else:
                print'''
[writeCoordinates says]: The file %s should have an extension *.nod.xml. Please rename it manually.
''' %(out_fileName)
        
def getEdgeList(fileName):
        '''
Function that gets the edge list and defines edge connectivity and returns it as a list.
        '''
        ## TODO: Extend the SNCF with data  also returns the edge list.
        edgeList=[]
        basName=os.path.basename(fileName)
        extName=basName.split('.')
        if extName[1]=='sncf' or extName=='SNCF':
                try:
                        fileHandler=open(fileName)
                        allLines=fileHandler.readlines()
                        numLines=sncfheaders.countLines(fileName)
                        strtNum=filesanity.findHeader('@start_elist_(edgName,frmId,toId)',fileName)
                        endNum=filesanity.findHeader('@end_elist',fileName)
                        # The reason why the interpreter prints the '@end_elist' line is because,
                        # there is a \n character (new line) in the end.
                        # So, endNum-1 should do the trick.
                        for itr in range (strtNum,endNum-1):
                            if allLines[itr]=='\n':
                                sncfheaders.readIgnore()
                            else:
                                edgeList.append(allLines[itr])
                        return edgeList
                except IOError,WindowsError:
                        print 'Error. Filename %s does not exist. Please check the name of the file and try again.' %(fileName)
        else:
                sys.stderr.write ('Error. The routines are performed on *.sncf files alone. Please make sure you use *.sncf files alone.')
                print 'You are using the filename %s.' %(fileName)
        return edgeList

def getEdgeIDs(inp_fileName):
        '''
Function that returns the edge id of each edge as a list. Input argument: the sncf filename.
        '''
        # Check if the file is empty.
        edgIDBuff=[]
        edgBuff=strutils.betweenHeaders('@start_elist_(edgName,frmId,toId)','@end_elist',inp_fileName)
        lenOf=len(edgBuff)
        for itr in range(0,lenOf):
                edgIDBuff.append(edgBuff[itr].split(',')[0])
        return edgIDBuff

def writeEdgeList(inp_fileName,out_fileName):
    '''
Function that writes edge list to an XML file. Requires an edge list to write to a file.
    '''
    nL=sncfheaders.printNew()
    xmlLine=''
    getEdges=getEdgeList(inp_fileName)
    lenEdgeList=len(getEdges)
    # Prepare the initial setup of the XML file
    xmlBegin='''<?xml version="1.0" encoding="UTF-8"?>
<edges xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:noNamespaceSchemaLocation="http://sumo.sf.net/xsd/edges_file.xsd">'''
    xmlEnd='</edges>'
    xmlEachEdgeFrom='   <edge from=\"'
    xmlEachEdgeTo='to=\"'
    xmlEachEdgeEnd='/>'
    xmlId='id=\"'
    fileHandler=open(out_fileName,'w')
    fileHandler.write(xmlBegin+nL)
    for itr in range(0,lenEdgeList):
            xmlLine=xmlEachEdgeFrom+str(getEdges[itr].split(',')[1])+'\"'+' '+xmlId+getEdges[itr].split(',')[0]+'\"'+' '+xmlEachEdgeTo+str(getEdges[itr].split(',')[2].split('\n')[0])+'\"'+xmlEachEdgeEnd+nL
            fileHandler.write(xmlLine)                                                                                                                           
    fileHandler.write(xmlEnd)
    fileHandler.close()
    baseName=os.path.basename(out_fileName)
    extName=os.path.splitext(baseName)[1]
    if extName=='.xml':
            print'''
[writeEdgeList says]: The file %s has been written to location %s.
            ''' %(out_fileName,os.getcwd())
    else:
            print'''
[writeEdgeList says]: The file %s should have an extension *.edg.xml. Please rename it manually.
            '''%(out_fileName)

def generateNetwork (nodFile,edgFile,out_netFile):
    '''
Function to generate the *.net.xml(out_netFile) file with *.nod.xml(nodFile) and *.edg.xml(edgFile) files as input.
    '''
##  Syntax for executing the edg and nod file to produce net file
##  netconvert --node-files=hello.nod.xml --edge-files=hello.edg.xml --output-file=hello.net.xml
    
    try:
        netconvertpath = globalasp.getPPath ('NETCONV')
        #This exists in 'D:\\Quest\\Aspire\\IDP\\sumo-winbin-0.16.0\\sumo-0.16.0\\bin\\netconvert.exe'
        genPath = netconvertpath+' '+'--node-files='+nodFile+' '+'--edge-files='+edgFile+' '+'--output-file='+out_netFile
        subprocess.check_output (genPath)
        print '''
[generateNetwork says]: The file %s has been written to location %s @ %s. 
                '''%(out_netFile,os.getcwd(),time.strftime("%d-%m-%Y %H:%M:%S"))
    except WindowsError,IOError:
        print
        ''' [generateNetwork says]: Error. Required program netconvert may not be installed or may not be located in <HOME-DIRECTORY>\SUMO\bin or the files %s and %s may not exist at all.
        ''' %(nodFile,edgFile)

def writeRoute(sncfFileName,outputFileName):
	'''
Function that writes the *.rou.xml file from an existing sncf file sncfFileName.
	'''
	
	if dependencychecker.depChecker(sncfFileName)==True:
		# Initial setup
		# Routes start and end
		routesStart='''<?xml version="1.0" encoding="UTF-8"?>
<routes>'''
		routesEnd='</routes>'
		# vType start and end
		vTypeID='<vType id=\"'
		vTypeAccel='\" accel=\"'
		vTypeDecel='\" decel=\"'
		vTypeSigma='\" sigma=\"'
		vTypeTau='\" tau=\"'
		vTypeLength='\" length=\"'
		vTypeminGap='\" minGap=\"'
		vTypemaxSpeed='\" maxSpeed=\"'
		vTypespeedFactor='\" speedFactor=\"'
		vTypespeedDev='\" speedDev=\"'
		vTypecolor='\" color=\"'
		vTypevClass='\" vClass=\"'
		vTypeemissionClass='\" emissionClass=\"'
		vTypeguiShape='\" guiShape=\"'
		vTypewidth='\" width=\"'
		vTypeimgFile='\" imgFile=\"'
		vTypeimpatience='\" impatience=\"'
		vTypelaneChangeModel='\" laneChangeModel=\"'
		vTypeEnd='\" />'
		# route start and end
		routeID='<route id=\"'
		routeColor='\" color=\"'
		routeEdges='\" edges=\"'
		routeEnd='\" />'
		# vehicle start and end
		vehicleID='< vehicle id=\"'
		vehicleType='\" type=\"'
		vehicleRoute='\" route=\"'
		vehicleColor='\" color=\"'
		vehicleDepart='\" depart=\"'
		vehicledepartLane='\" departLane=\"'
		vehicledepartPos='\" departPos=\"'
		vehicledepartSpeed='\" departSpeed=\"'
		vehiclearrivalLane='\" arrivalLane=\"'
		vehiclearrivalPos='\" arrivalPos=\"'
		vehiclearrivalSpeed='\" arrivalSpeed=\"'
		vehicleline='\" line=\"'
		vehicleEnd='\" />'
		lenVtype=len(getattribs.getvTypeAttribs(sncfFileName))
		lenRoute=len(getattribs.getrouteEntryAttribs(sncfFileName))
		lenVehicle=len(getattribs.getvehicleAttribs(sncfFileName))
		# Begin writing the file
		try:
			fileHandler=open(outputFileName,'w')
			fileHandler.write(routesStart+sncfheaders.printNew())
			# Returns the list containing vType values.
			vType=getattribs.getvTypeDefaults(sncfFileName)
			for itr in range(0,lenVtype):
				vTypeWrite=vTypeID+vType[itr][0]+vTypeAccel+vType[itr][1]+vTypeDecel+vType[itr][2]+vTypeSigma+vType[itr][3]+vTypeTau+vType[itr][4]+vTypeLength+vType[itr][5]+vTypeminGap+vType[itr][6]+vTypemaxSpeed+vType[itr][7]+vTypespeedFactor+vType[itr][8]+vTypespeedDev+vType[itr][9]+vTypecolor+getattribs.getvTypeColor(sncfFileName)[itr]+vTypevClass+vType[itr][11]+vTypeemissionClass+vType[itr][12]+vTypeguiShape+vType[itr][13]+vTypewidth+vType[itr][14]+vTypeimgFile+vType[itr][15]+vTypeimpatience+vType[itr][16]+vTypelaneChangeModel+vType[itr][17]+vTypeEnd+sncfheaders.printNew()
				fileHandler.write(vTypeWrite)
				# Returns the list containing route entry values.
			routeEntry=getattribs.getrouteEntryAttribs(sncfFileName)
			for itr in range(0,lenRoute):
				routeWrite=routeID+routeEntry[itr][0]+routeColor+getattribs.getrouteColor(sncfFileName)[itr]+routeEdges+strutils.whatsInBetween('[',']',getattribs.getroute_edgeName(sncfFileName)[itr])+routeEnd+sncfheaders.printNew()
				fileHandler.write(routeWrite)
				# Returns the list containing vehicle values.
			vehicle=getattribs.getvehicleAttribs(sncfFileName)
			for itr in range(0,lenVehicle):
				vehicleWrite=vehicleID+vehicle[itr][0]+vehicleType+vehicle[itr][1]+vehicleRoute+vehicle[itr][2]+vehicleColor+getattribs.getvehicleColor(sncfFileName)[itr]+vehicleDepart+vehicle[itr][4]+vehicledepartLane+vehicle[itr][5]+vehicledepartPos+vehicle[itr][6]+vehicledepartSpeed+vehicle[itr][7]+vehiclearrivalLane+vehicle[itr][8]+vehiclearrivalPos+vehicle[itr][9]+vehiclearrivalSpeed+vehicle[itr][10]+vehicleline+vehicle[itr][11]+vehicleEnd+sncfheaders.printNew()
				fileHandler.write(vehicleWrite)
			fileHandler.write(routesEnd+sncfheaders.printNew())
			fileHandler.close()
			print'''
[writeRoute says]:The route file %s has been written to the location %s.
			'''%(outputFileName,os.getcwd())
		except IOError,WindowsError:
			print'''
[writeRoute says]:Error! Could not open the file %s for writing. Check the administrative rights or check if the location exists.
			'''%(outputFileName)
	else:
		print'''
[writeRoute says]:Error generating %s. The dependency check failed. Check the sncf file %s for appropriateness. 
		'''%(outputFileName,sncfFileName)
