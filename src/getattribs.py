#!C:\Python27
#!/usr/bin/python
################################### 
# Author:Nitin Deshpande
# Filename:getattribs.py
# Extract sub-attributes(meta-data) from the main attributes.
# Date: 2013-09-21
###################################
import strutils
import os
import sys
import filesanity

def getvTypeAttribs(sncfFileName):
	'''
Function that returns the value of the meta-data of the vType attribute. There are 18 meta-data variables.
Hence, getvTypeAttribs(sncfFileName)[1] returns the acceleration value 'accel'.
	'''
	vTypeBuff=[]
	vTypeBet=strutils.betweenHeaders('@start_vTypeID_({vTypeID,accel,decel,sigma,tau,length,minGap,maxSpeed,speedFactor,speedDev,[R G B],vClass,emissionClass,guiShape,width,imgFile,impatience,laneChangeModel})','@end_vTypeID',sncfFileName)
	lenBuff=len(vTypeBet)
	for itr in range(0,lenBuff):
		buffInBet=strutils.whatsInBetween('{','}',vTypeBet[itr])
		vTypeBuff.append(buffInBet.split(','))
	return vTypeBuff
		
def getrouteEntryAttribs(sncfFileName):
	'''
Function that returns the value of the meta-data of the routeEntry attribute. getrouteEntryAttribs(sncfFileName)[1] returns the color triad value.
	'''
	routeBuff=[]
	routeEntryBet=strutils.betweenHeaders('@start_routeentry_({routeID,[R G B],[edgName_a edgName_b edgName_c  edgName_n]})','@end_routeentry',sncfFileName)
	lenBuff=len(routeEntryBet)
	for itr in range(0,lenBuff):
		buffInBet=strutils.whatsInBetween('{','}',routeEntryBet[itr])
		routeBuff.append(buffInBet.split(','))
	return routeBuff

def getvehicleAttribs(sncfFileName):
	'''
Function that returns the value of the meta-data of the vehicle attribute. getvehicleAttribs(sncfFileName)[1] returns the vehicle type ID.
	'''
	vehicleBuff=[]
	vehicleBet=strutils.betweenHeaders('@start_vehID_({uID,vTypeID,routeID,[R G B],entryTime,entryLane,entryPtPos,entrySpeed,exitLane,exitPos,exitSpeed,line})','@end_vehID',sncfFileName)
	lenBuff=len(vehicleBet)
	for itr in range(0,lenBuff):
		buffInBet=strutils.whatsInBetween('{','}',vehicleBet[itr])
		vehicleBuff.append(buffInBet.split(','))
	return vehicleBuff
###################################################################################
# [Test A] vType.ID=vehicle.type
# vType=vTypeAttribs
# veh=vehicleAttribs
# route=routeEntryAttribs
def getvType_vTypeID(sncfFileName):
	'''
Function that returns a list of vehicle type id(s) from vType attribute from the sncf file sncfFileName.
	'''
	if filesanity.check_vTypeAttribs(sncfFileName) and filesanity.check_routeEntryAttribs(sncfFileName) and filesanity.check_vehicleAttribs(sncfFileName):
		vTypeList=[]
		for i in range(0,len(getvTypeAttribs(sncfFileName))):
			vTypeList.append(getvTypeAttribs(sncfFileName)[i][0])
		return vTypeList
	else:
		print'''
[getvType_vTypeID says]:Error!The sncf file %s may be corrupted. Use a text editor to enclose each set of attributes inside a pair of curly brackets.Or, check if the field contains 18 attribute values.	
		'''%sncfFileName
		
def getveh_vTypeID(sncfFileName):
	'''
Function that returns a list of vehicle id(s) from vehicle attributes from the file sncfFileName.
	'''
	if filesanity.check_vTypeAttribs(sncfFileName) and filesanity.check_routeEntryAttribs(sncfFileName) and filesanity.check_vehicleAttribs(sncfFileName):
		vehList=[]
		for i in range(0,len(getvehicleAttribs(sncfFileName))):
			vehList.append(getvehicleAttribs(sncfFileName)[i][1])
		return vehList
	else:
		print'''
[getveh_vTypeID says]:Error!The sncf file %s may be corrupted. Use a text editor to enclose each set of attributes inside a pair of curly brackets.Or, check if the field contains 12 attribute values.	
		'''%sncfFileName
###################################################################################
# [Test B] route.ID=vehicle.route
# vType=vTypeAttribs
# veh=vehicleAttribs
# route=routeEntryAttribs
def getroute_routeID(sncfFileName):
	'''
Function that returns a list of route id(s) from route entry attributes from the file sncfFileName.
	'''
	if filesanity.check_vTypeAttribs(sncfFileName) and filesanity.check_routeEntryAttribs(sncfFileName) and filesanity.check_vehicleAttribs(sncfFileName):
		rList=[]
		for i in range(0,len(getrouteEntryAttribs(sncfFileName))):
			rList.append(getrouteEntryAttribs(sncfFileName)[i][0])
		return rList
	else:
		print'''
[getroute_routeID says]:Error!The sncf file %s may be corrupted. Use a text editor to enclose each set of attributes inside a pair of curly brackets.Or, check if the field contains 3 attribute values.	
		'''%sncfFileName

def getveh_routeID(sncfFileName):
	'''
Function that returns a list of route id(s) from vehicle attributes from the file sncfFileName.
	'''
	if filesanity.check_vTypeAttribs(sncfFileName) and filesanity.check_routeEntryAttribs(sncfFileName) and filesanity.check_vehicleAttribs(sncfFileName):
		vrList=[]
		for i in range(0,len(getvehicleAttribs(sncfFileName))):
			vrList.append(getvehicleAttribs(sncfFileName)[i][2])
		return vrList
	else:
		print'''
[getveh_routeID says]:Error!The sncf file %s may be corrupted. Use a text editor to enclose each set of attributes inside a pair of curly brackets.Or, check if the field contains 3 attribute values.	
		'''%sncfFileName
		
def getroute_edgeName(sncfFileName):
	'''
Function that returns a list of possible routes the vehicles can take from the file sncfFileName.
	'''
	if filesanity.check_vTypeAttribs(sncfFileName) and filesanity.check_routeEntryAttribs(sncfFileName) and filesanity.check_vehicleAttribs(sncfFileName):
		reList=[]
		for i in range(0,len(getrouteEntryAttribs(sncfFileName))):
			reList.append(getrouteEntryAttribs(sncfFileName)[i][2])
		return reList
	else:
		print'''
[getroute_edgeName says]:Error!The sncf file %s may be corrupted. Use a text editor to enclose each set of attributes inside a pair of curly brackets.Or, check if the field contains 3 attribute values.	
		'''%sncfFileName
		
def getvectEdgeNames(sncfFileName):
	'''
Function that returns the edge names used in the route attributes as an alphabetically sorted vector. 
	'''
	instrList=[]
	sepList2d=[]
	vectorEdgeNames=[]
	edgeNames=getroute_edgeName(sncfFileName)
	# Gets the string in between the [ and ]
	for itr in range(0,len(getroute_edgeName(sncfFileName))):
		instrList.append(strutils.whatsInBetween('[',']',edgeNames[itr]))
	# Gets the 2d list of the strings separated by spaces
	for itr in range(0,len(getroute_edgeName(sncfFileName))):
		edgeName2d=instrList[itr]
		sepList2d.append(edgeName2d.split(' '))
	# This part does the vectorisation part. Examines every element in the 2d array.
	for itr in range(0,len(sepList2d)):
		# Get the length of each element in the 2d array to sort of dynamically adjust the iteration counter
		len_sepList2d=len(sepList2d[itr])
		for jtr in range(0,len_sepList2d):
			# Dynamically adjusted iteration and store the result in a vector. Done!
			vectorEdgeNames.append(sepList2d[itr][jtr])
	return vectorEdgeNames

def getvTypeDefaults(sncfFileName):
	'''
Function that returns a list of vType attribute default values, if specified as 'default'. 
	'''
	try:
		if filesanity.check_vTypeAttribs(sncfFileName)==True:
			listGet=getvTypeAttribs(sncfFileName)
			for i in range(0,len(listGet)):
				if listGet[i][1]=='default':
					listGet[i][1]='2.60'
				else:
					pass
				if listGet[i][2]=='default':
					listGet[i][2]='4.50'
				else:
					pass
				if listGet[i][3]=='default':
					listGet[i][3]='0.5'
				else:
					pass
				if listGet[i][4]=='default':
					listGet[i][4]='1.0'
				else:
					pass
				if listGet[i][5]=='default':
					listGet[i][5]='5.0'
				else:
					pass
				if listGet[i][6]=='default':
					listGet[i][6]='2.50'
				else:
					pass
				if listGet[i][7]=='default':
					listGet[i][7]='70.0'
				else:
					pass
				if listGet[i][8]=='default':
					listGet[i][8]='1.0'
				else:
					pass
				if listGet[i][9]=='default':
					listGet[i][9]='0.0'
				else:
					pass
				if listGet[i][10]=='default':
					listGet[i][10]='[1 1 0]'
				else:
					pass
				if listGet[i][11]=='default':
					listGet[i][11]='unknown'
				else:
					pass
				if listGet[i][12]=='default':
					listGet[i][12]='P_7_7'
				else:
					pass
				if listGet[i][13]=='default':
					listGet[i][13]='unknown'
				else:
					pass
				if listGet[i][14]=='default':
					listGet[i][14]='2.0'
				else:
					pass
				if listGet[i][15]=='default':
					listGet[i][15]=''
				else:
					pass
				if listGet[i][16]=='default':
					listGet[i][16]='0.0'
				else:
					pass
				if listGet[i][17]=='default':
					listGet[i][17]='DK2008'
				else:
					pass
			return listGet
		else:
			return None	
	except TypeError:
		print'''
[getvTypeDefaults says]:Error! Unable to determine defaults. Check the sncf file %s and try again.
		'''%sncfFileName

def getvTypeColor(sncfFileName):
	'''
Function that returns the color triad of vType attribute.
	'''
	listFinal=[]
	lenAttribs=len(getvTypeAttribs(sncfFileName))
	for itr in range(0,lenAttribs):
		buffAttribs=getvTypeAttribs(sncfFileName)[itr][10]
		inResult=strutils.whatsInBetween('[',']',buffAttribs)
		listFinal.append(inResult.replace(' ',','))
	return listFinal

def getrouteColor(sncfFileName):
	'''
Function that returns the color triad of route entry attribute.
	'''
	listFinal=[]
	lenAttribs=len(getrouteEntryAttribs(sncfFileName))
	for itr in range(0,lenAttribs):
		buffAttribs=getrouteEntryAttribs(sncfFileName)[itr][1]
		inResult=strutils.whatsInBetween('[',']',buffAttribs)
		listFinal.append(inResult.replace(' ',','))
	return listFinal

def getvehicleColor(sncfFileName):
	'''
Function that returns the color triad of vehicle attribute.
	'''
	listFinal=[]
	lenAttribs=len(getvehicleAttribs(sncfFileName))
	for itr in range(0,lenAttribs):
		buffAttribs=getvehicleAttribs(sncfFileName)[itr][3]
		inResult=strutils.whatsInBetween('[',']',buffAttribs)
		listFinal.append(inResult.replace(' ',','))
	return listFinal